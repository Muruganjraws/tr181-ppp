/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>

#include "dm_ppp.h"
#include "ppp_utils.h"
#include "ppp_soc_utils.h"
#include "ppp_subscription_utils.h"

#define ME "ppp"

amxd_status_t _Reset(amxd_object_t* object,
                     UNUSED amxd_function_t* func,
                     UNUSED amxc_var_t* args,
                     UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    amxc_var_t params;
    ppp_info_t* ppp = NULL;

    amxc_var_init(&params);
    when_null_trace(object, exit, ERROR, "Object is null");
    when_null_trace(object->priv, exit, ERROR, "Object private data is null");

    ppp = (ppp_info_t*) object->priv;
    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    if(GET_BOOL(&params, "Enable")) {
        SAH_TRACEZ_INFO(ME, "Resetting PPP Interface %s", GET_CHAR(&params, "Alias"));
        when_failed_trace(mod_ppp_execute_function("reset", ppp), exit, ERROR,
                          "Failed to execute reset");
        when_failed(start_ppp_timer(ppp, false), exit)
    } else {
        SAH_TRACEZ_INFO(ME, "PPP Interface %s is not enabled, not doing Reset", GET_CHAR(&params, "Alias"));
    }

exit:
    amxc_var_clean(&params);
    return status;
}

void _ppp_interface_added(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    ppp_info_t* ppp = NULL;
    amxd_object_t* ppp_templ_obj = NULL;
    amxd_object_t* intf_obj = NULL;
    amxd_dm_t* dm = ppp_get_dm();
    amxd_status_t rv = amxd_status_unknown_error;

    ppp_templ_obj = amxd_dm_signal_get_object(dm, data);
    when_null_trace(ppp_templ_obj, exit, ERROR, "could not get ppp interface template object");

    intf_obj = amxd_object_get_instance(ppp_templ_obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(intf_obj, exit, ERROR,
                    "could not get ppp interface with index %d", GET_UINT32(data, "index"));

    when_not_null(intf_obj->priv, exit);

    rv = ppp_info_new(&ppp);
    when_failed(rv, exit);

    ppp->intf_obj = intf_obj;
    ppp->intf_obj->priv = ppp;

    if(mod_ppp_execute_function("use-timers", ppp) != 0) {
        ppp->use_timers = true;
        amxp_timer_new(&(ppp->enable_timer), enable_callback, ppp);
        amxp_timer_new(&(ppp->disable_timer), disable_callback, ppp);
    } else {
        ppp->use_timers = false;
        SAH_TRACEZ_INFO(ME, "not starting ppp timers");
    }

    if(mod_ppp_execute_function("use-netdev-subscriptions", ppp) != 0) {
        handle_netdev_subscriptions(ppp);
    }

    set_intf_info(ppp);

    SAH_TRACEZ_INFO(ME, "new ppp interface %s added",
                    amxd_object_get_name(intf_obj, AMXD_OBJECT_NAMED));


exit:
    return;
}

void _ppp_interface_enable_changed(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    ppp_info_t* ppp = NULL;
    amxd_object_t* obj = amxd_dm_signal_get_object(ppp_get_dm(), data);
    when_null(obj, exit);
    ppp = (ppp_info_t*) obj->priv;
    set_intf_info(ppp);

    if(!amxd_object_get_value(bool, obj, "Enable", NULL)) {
        when_failed_trace(mod_ppp_execute_function("update-config", ppp), exit, ERROR,
                          "Failed to update config for PPP Interface");
        when_failed(start_ppp_timer(ppp, false), exit)
    } else {
        when_failed(start_ppp_timer(ppp, true), exit)
    }

exit:
    return;
}

void _ppp_interface_lowerlayers_changed(UNUSED const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    ppp_info_t* ppp = NULL;
    amxd_object_t* obj = amxd_dm_signal_get_object(ppp_get_dm(), data);
    when_null(obj, exit);
    ppp = (ppp_info_t*) obj->priv;
    cleanup_nm_interface_query(ppp);
    set_intf_info(ppp);

exit:
    return;
}

static void interface_changed(amxd_object_t* ppp_interface_obj) {
    ppp_info_t* ppp = NULL;
    amxc_var_t params;

    amxc_var_init(&params);
    when_null_trace(ppp_interface_obj, exit, ERROR, "could not get ppp interface object");
    amxd_object_get_params(ppp_interface_obj, &params, amxd_dm_access_protected);

    ppp = (ppp_info_t*) ppp_interface_obj->priv;
    when_null_trace(ppp, exit, ERROR, "ppp interface private data is null");

    when_failed_trace(mod_ppp_execute_function("update-config", ppp), exit, ERROR,
                      "Failed to update config for PPP Interface");

    if(GETP_BOOL(&params, "Enable") &&
       ( strcmp(GETP_CHAR(&params, "ConnectionStatus"), "Connected") != 0) &&
       ( strcmp(GETP_CHAR(&params, "Status"), "Up") != 0)) {
        when_failed(start_ppp_timer(ppp, true), exit)
    }

    if(!GETP_BOOL(&params, "Enable") &&
       ( strcmp(GETP_CHAR(&params, "ConnectionStatus"), "Connected") == 0) &&
       ( strcmp(GETP_CHAR(&params, "Status"), "Up") == 0)) {
        when_failed(start_ppp_timer(ppp, false), exit)
    }

exit:
    amxc_var_clean(&params);
    return;
}

void _ppp_interface_changed(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    amxd_object_t* ppp_interface_obj = amxd_dm_signal_get_object(ppp_get_dm(), data);
    interface_changed(ppp_interface_obj);
}

void _ppp_pppoe_interface_changed(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    amxd_object_t* pppoe_obj = amxd_dm_signal_get_object(ppp_get_dm(), data);
    amxd_object_t* ppp_interface_obj = amxd_object_get_parent(pppoe_obj);
    interface_changed(ppp_interface_obj);
}


amxd_status_t _update(UNUSED amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    amxc_var_t* data = NULL;
    const char* mod_name = NULL;
    const char* mod_namespace = NULL;
    const char* func_name = NULL;
    int retval = 1;
    amxd_status_t stat = amxd_status_invalid_function_argument;

    mod_name = GET_CHAR(args, "mod_name");
    mod_namespace = GET_CHAR(args, "mod_namespace");
    func_name = GET_CHAR(args, "fnc");
    data = GET_ARG(args, "data");

    when_null_trace(mod_name, exit, ERROR, "Mod name is null");
    when_null_trace(mod_namespace, exit, ERROR, "Mod namespace is null");
    when_null_trace(func_name, exit, ERROR, "Function name is null");
    when_null_trace(data, exit, ERROR, "Data is null");

    stat = amxd_status_unknown_error;
    retval = amxm_execute_function(mod_name, mod_namespace, func_name, data, ret);
    SAH_TRACEZ_INFO(ME, "Mod %s, func '%s' returned %d", mod_name, func_name, retval);

    if(retval == 0) {
        stat = amxd_status_ok;
    }

exit:
    return stat;
}
