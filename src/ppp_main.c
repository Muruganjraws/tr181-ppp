/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ppp.h"
#include "ppp_utils.h"
#include "netmodel/client.h"

#include <string.h>

#define ME "ppp"
#define string_empty(X) ((X == NULL) || (*X == '\0'))

static ppp_app_t ppp;
static amxc_var_t active_fds;

amxc_var_t* get_active_fds(void);

amxd_dm_t* PRIVATE ppp_get_dm(void) {
    return ppp.dm;
}

amxo_parser_t* PRIVATE ppp_get_parser(void) {
    return ppp.parser;
}

amxc_var_t* get_active_fds(void) {
    return &active_fds;
}

/*
 * Callback function used for the mod-ppp-daemon implementation when data is available to read.
 */
static void read_handler(UNUSED int fd, void* data) {
    amxc_var_t* var = (amxc_var_t*) data;
    amxc_var_t ret;
    const char* function = GET_CHAR(var, "handler");

    when_str_empty_trace(function, exit, ERROR, "Module function missing");

    amxc_var_init(&ret);

    amxm_execute_function("mod-ppp-daemon", "ppp-ctrl", function, var, &ret);

    amxc_var_clean(&ret);

exit:
    return;
}

static int ppp_event_loop_cleanup_fds(UNUSED const char* function_name,
                                      UNUSED amxc_var_t* args,
                                      UNUSED amxc_var_t* ret) {
    when_true(amxc_var_is_null(&active_fds), exit);

    amxc_var_for_each(entry, &active_fds) {
        amxc_var_t* file_descriptors = GET_ARG(entry, "file_descriptors");
        amxc_var_for_each(fd, file_descriptors) {
            amxo_connection_remove(ppp_get_parser(), amxc_var_constcast(fd_t, fd));
        }
    }
    amxc_var_clean(&active_fds);
exit:
    return 0;
}

static int ppp_event_loop_add_fd(UNUSED const char* function_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    // Initialze active_fds when it is cleared.
    if(amxc_var_is_null(&active_fds)) {
        amxc_var_init(&active_fds);
        amxc_var_set_type(&active_fds, AMXC_VAR_ID_HTABLE);
    }

    amxc_var_t* existing_entry = NULL;
    const char* key = GET_CHAR(args, "handler");
    amxc_var_t* new_file_descriptors = GET_ARG(args, "file_descriptors");

    if(string_empty(key)) {
        SAH_TRACEZ_WARNING(ME, "Cannot add fd to event loop without 'handler'");
        return -1;
    }

    existing_entry = amxc_var_get_key(&active_fds, key, AMXC_VAR_FLAG_DEFAULT);
    if(NULL != existing_entry) {
        amxc_var_t* old_file_descriptors = GET_ARG(existing_entry, "file_descriptors");

        int result = 0;
        int rc = amxc_var_compare(old_file_descriptors, new_file_descriptors, &result);
        when_failed(rc, exit);
        if(result == 0) {
            // file descriptors are the same, skip this.
            goto exit;
        }
        SAH_TRACEZ_NOTICE(ME, "Remove fd[%s] from event loop", GET_CHAR(existing_entry, "handler"));
        amxc_var_for_each(fd, old_file_descriptors) {
            amxo_connection_remove(ppp_get_parser(), amxc_var_constcast(fd_t, fd));
        }
    }

    bool all_new_fds_valid = true;
    amxc_var_for_each(fd, new_file_descriptors) {
        if(amxc_var_constcast(fd_t, fd) <= 0) {
            all_new_fds_valid = false;
            break;
        }
    }

    if(all_new_fds_valid) {
        amxo_con_type_t type = AMXO_CUSTOM;
        amxc_var_t* type_arg = NULL;

        type_arg = GET_ARG(args, "type");
        if(NULL != type_arg) {
            type = (amxo_con_type_t) amxc_var_constcast(uint32_t, type_arg);
        }

        if(NULL == existing_entry) {
            existing_entry = amxc_var_add_new_key(&active_fds, key);
        }
        amxc_var_move(existing_entry, args);

        SAH_TRACEZ_NOTICE(ME, "Add fd[%s] to event loop", key);
        amxc_var_for_each(fd, new_file_descriptors) {
            amxo_connection_add(ppp_get_parser(), amxc_var_constcast(fd_t, fd), read_handler, NULL, type, existing_entry);
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "Not all file descriptors are valid so not adding them to amx eventloop.");
    }
exit:
    return 0;
}

static int ppp_load_controllers(void) {
    int rv = -1;
    const amxc_var_t* controllers = NULL;
    amxc_var_t lcontrollers;
    amxc_string_t mod_path;
    amxm_shared_object_t* so = NULL;

    amxd_object_t* ppp_obj = amxd_dm_findf(ppp_get_dm(), "PPP");
    controllers = amxd_object_get_param_value(ppp_obj, "SupportedControllers");

    amxc_var_t mod_dirs;
    amxc_string_t mod_dirs_csv;
    amxc_var_init(&mod_dirs);
    amxc_string_init(&mod_dirs_csv, 0);
    amxc_string_set(&mod_dirs_csv, GETP_CHAR(&(ppp_get_parser()->config), "tr181-ppp.mod-dirs"));
    amxc_string_csv_to_var(&mod_dirs_csv, &mod_dirs, NULL);

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);

        bool found = false;
        SAH_TRACEZ_INFO(ME, "Loading controller '%s'...", name);
        amxc_var_for_each(mod_dir, &mod_dirs) {
            amxc_string_setf(&mod_path, "%s/%s.so", GET_CHAR(mod_dir, NULL), name);
            rv = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
            if(rv == 0) {
                found = true;
                break;
            }
            SAH_TRACEZ_INFO(ME, "Loading controller '%s' failed (file = %s)", name, amxc_string_get(&mod_path, 0));
        }
        if(found) {
            ppp.mod_found = true;
            SAH_TRACEZ_INFO(ME, "Loading controller '%s' successful (file = %s)", name, amxc_string_get(&mod_path, 0));
        }
    }

    if(ppp.mod_found) {
        rv = 0;
    } else {
        // try to load the dummy module
        amxc_var_for_each(mod_dir, &mod_dirs) {
            amxc_string_setf(&mod_path, "%s/%s.so", GET_CHAR(mod_dir, NULL), DUMMY_MOD_NAME);
            rv = amxm_so_open(&so, DUMMY_MOD_NAME, amxc_string_get(&mod_path, 0));
            if(rv == 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to load ppp modules, dummy module will be used");
                break;
            }
        }
        when_failed_trace(rv, exit, ERROR, "Failed to load any ppp module, the plugin will not be started");
    }

exit:
    amxc_string_clean(&mod_path);
    amxc_string_clean(&mod_dirs_csv);
    amxc_var_clean(&lcontrollers);
    amxc_var_clean(&mod_dirs);
    return rv;
}

static int ppp_register_core_mod(void) {
    int retval = -1;
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    when_null(so, exit);

    when_failed_trace(amxm_module_register(&mod, so, MOD_CORE), exit, ERROR,
                      "Failed to register mod %s", MOD_CORE);

    retval = amxm_module_add_function(mod, "update-dm", ppp_update_dm);
    retval |= amxm_module_add_function(mod, "event-loop-add-fd", ppp_event_loop_add_fd);
    retval |= amxm_module_add_function(mod, "event-loop-cleanup-fds", ppp_event_loop_cleanup_fds);

exit:
    return retval;
}

static int ppp_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int rv = -1;

    SAH_TRACEZ_INFO(ME, "PPP manager start");
    ppp.dm = dm;
    ppp.parser = parser;
    ppp.mod_found = false;
    when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to initialize libnetmodel");

    rv = amxo_parser_parse_string(parser, "?include '${odl.directory}/${name}.odl':'${odl.dm-defaults}';",
                                  amxd_dm_get_root(dm));
    rv |= ppp_load_controllers();
    rv |= ppp_register_core_mod();

    dm_storage_init();

exit:
    return rv;
}

static int ppp_clean(void) {
    ppp.dm = NULL;
    ppp.parser = NULL;

    ppp_event_loop_cleanup_fds(NULL, NULL, NULL);

    dm_storage_clean();
    amxm_close_all();
    netmodel_cleanup();
    SAH_TRACEZ_INFO(ME, "PPP manager stop");

    return 0;
}

int _ppp_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int rv = 0;

    switch(reason) {
    case AMXO_START:
        rv = ppp_init(dm, parser);
        break;
    case AMXO_STOP:
        rv = ppp_clean();
        break;
    default:
        rv = -1;
    }
    return rv;
}
