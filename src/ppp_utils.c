/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>

#include "ppp.h"
#include "ppp_utils.h"
#include "ppp_soc_utils.h"
#include "ppp_subscription_utils.h"

#define ME "ppp_utils"

static amxc_var_t dm_storage;

int start_ppp_timer(ppp_info_t* ppp, bool enable) {
    int rv = 0;
    uint32_t timeout = GETP_UINT32(&(ppp_get_parser()->config), "tr181-ppp.connection-timeout");

    when_false(ppp->use_timers, exit)

    rv = amxp_timer_start(enable ? ppp->enable_timer : ppp->disable_timer, timeout);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to start %s timer", enable ? "enable" : "disable"); goto exit;
    }

exit:
    return rv;
}

int stop_ppp_timer(ppp_info_t* ppp, bool enable) {
    int rv = 0;

    when_false(ppp->use_timers, exit)

    rv = amxp_timer_stop(enable ? ppp->enable_timer : ppp->disable_timer);

exit:
    return rv;
}

static amxd_object_t* find_ppp_obj_by_device(const char* device) {
    amxd_object_t* instance = NULL;
    amxd_object_t* template_obj = amxd_dm_findf(ppp_get_dm(), "PPP.Interface.");

    when_null_trace(template_obj, exit, ERROR, "Could not find PPP.Interface. template object");

    amxd_object_for_each(instance, it, template_obj) {
        const char* inst_device = NULL;
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        if(inst->priv == NULL) {
            continue;
        }

        inst_device = ((ppp_info_t*) inst->priv)->ll_intf_name;
        if(inst_device == NULL) {
            continue;
        }

        if(strcmp(inst_device, device) == 0) {
            instance = inst;
            break;
        }
    }

exit:
    return instance;
}

void enable_callback(UNUSED amxp_timer_t* timer, void* priv) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    uint32_t timeout = GETP_UINT32(&(ppp_get_parser()->config), "tr181-ppp.connection-timeout");

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    SAH_TRACEZ_ERROR(ME, "Failed to enable PPP connection after %ums", timeout);

    when_null_trace(priv, exit, ERROR, "Private data is null");
    obj = ((ppp_info_t*) priv)->intf_obj;
    when_null_trace(obj, exit, ERROR, "PPP Interface object is null");

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Error");
    amxd_trans_set_value(cstring_t, &trans, "LastConnectionError", "ERROR_UNKNOWN");
    when_failed_trace(amxd_trans_apply(&trans, ppp_get_dm()), exit, ERROR,
                      "Failed to update datamodel");

exit:
    amxd_trans_clean(&trans);
    return;
}

void disable_callback(UNUSED amxp_timer_t* timer, void* priv) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    uint32_t timeout = GETP_UINT32(&(ppp_get_parser()->config), "tr181-ppp.connection-timeout");

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    SAH_TRACEZ_ERROR(ME, "Failed to disable PPP connection after %ums", timeout);

    when_null_trace(priv, exit, ERROR, "Private data is null");
    obj = ((ppp_info_t*) priv)->intf_obj;
    when_null_trace(obj, exit, ERROR, "PPP Interface object is null");

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Error");
    when_failed_trace(amxd_trans_apply(&trans, ppp_get_dm()), exit, ERROR,
                      "Failed to update datamodel");

exit:
    amxd_trans_clean(&trans);
    return;
}

static void client_intf_netmodel_cb(UNUSED const char* sig_name,
                                    const amxc_var_t* data,
                                    void* priv) {
    ppp_info_t* ppp = (ppp_info_t*) priv;
    const char* intf_name = NULL;
    amxc_var_t* stored_data = NULL;
    amxc_var_t ret;
    int retval = -1;

    amxc_var_init(&ret);
    when_null(data, exit);
    when_null(ppp, exit);
    intf_name = amxc_var_constcast(cstring_t, data);
    SAH_TRACEZ_ERROR(ME, "intf_name %s -> %s",
                     ppp->ll_intf_name, intf_name);

    free(ppp->ll_intf_name);
    ppp->ll_intf_name = NULL;

    if((intf_name != NULL) && (intf_name[0] != '\0')) {
        ppp->ll_intf_name = strdup(intf_name);
    }

    // It is possible that the interface name query returns after pppd calls the ppp-up script at boot
    // Check if there is stored data for the new interface name
    stored_data = dm_storage_get_data(intf_name);
    if(stored_data != NULL) {
        SAH_TRACEZ_INFO(ME, "Found stored ppp data for device %s, updating dm", intf_name);
        retval = amxm_execute_function("self", MOD_CORE, "update-dm", stored_data, &ret);
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "Mod %s, func 'update-dm' returned %d",
                               MOD_CORE, retval);
        }
        amxc_var_delete(&stored_data);
    }

    when_failed_trace(mod_ppp_execute_function("update-config", ppp), exit, ERROR,
                      "Failed to update config for PPP Interface");
exit:
    amxc_var_clean(&ret);
    return;
}

static void remove_interface_name(ppp_info_t* ppp) {
    when_null(ppp, exit);
    free(ppp->ll_intf_name);
    ppp->ll_intf_name = NULL;

exit:
    return;
}

void cleanup_nm_interface_query(ppp_info_t* ppp) {
    if(ppp->q_name != NULL) {
        netmodel_closeQuery(ppp->q_name);
        ppp->q_name = NULL;
    }
}

void set_intf_info(ppp_info_t* ppp) {
    amxd_object_t* ppp_object = ppp->intf_obj;
    bool enable = amxd_object_get_value(bool, ppp_object, "Enable", NULL);
    char* lowerlayer_intf = amxd_object_get_value(cstring_t, ppp_object, "LowerLayers", NULL);

    when_null(ppp_object, exit);

    if(enable) {
        amxc_string_t ll;
        amxc_string_t flags;

        amxc_string_init(&ll, 0);
        amxc_string_init(&flags, 0);
        amxc_string_set(&ll, lowerlayer_intf);
        amxc_string_set(&flags, "netdev-up");

        if(amxc_string_search(&ll, "Ethernet.VLANTermination.", 0) >= 0) {
            amxc_string_appendf(&flags, " %s", "vlan");
        }

        ppp->q_name = netmodel_openQuery_getFirstParameter((const char*) lowerlayer_intf,
                                                           "tr181-ppp", "NetDevName", amxc_string_get(&flags, 0),
                                                           netmodel_traverse_down,
                                                           client_intf_netmodel_cb, (void*) ppp);
        if(ppp->q_name != NULL) {
            SAH_TRACEZ_INFO(ME, "Query to get NetDevName for intf %s ok",
                            lowerlayer_intf);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to add a query to netmodel to get NetDevName");
        }

        amxc_string_clean(&flags);
        amxc_string_clean(&ll);
    } else {
        cleanup_nm_interface_query(ppp);
    }

exit:
    free(lowerlayer_intf);
    return;
}

void handle_netdev_subscriptions(ppp_info_t* ppp) {
    amxd_object_t* ppp_object = NULL;

    when_null(ppp, exit);
    ppp_object = ppp->intf_obj;
    when_null(ppp_object, exit);

    netdev_link_state_subscription_new(ppp);
    netdev_link_added_subscription_new(ppp);
    netdev_link_removed_subscription_new(ppp);

exit:
    return;
}

amxd_status_t ppp_info_new(ppp_info_t** ppp) {
    amxd_status_t rv = amxd_status_invalid_function_argument;

    when_null_trace(ppp, exit, ERROR, "ppp can not be null, invalid argument");

    *ppp = (ppp_info_t*) calloc(1, sizeof(ppp_info_t));
    when_null_status(*ppp, exit, rv = amxd_status_out_of_mem);
    (*ppp)->last_change = get_system_uptime();

    rv = amxd_status_ok;
exit:
    return rv;
}

amxd_status_t ppp_info_clean(ppp_info_t** ppp) {
    if((ppp == NULL) || (*ppp == NULL)) {
        goto exit;
    }
    cleanup_nm_interface_query(*ppp);
    netdev_cleanup(*ppp);
    remove_interface_name(*ppp);

    amxp_timer_delete(&((*ppp)->enable_timer));
    amxp_timer_delete(&((*ppp)->disable_timer));
    free(*ppp);
    *ppp = NULL;

exit:
    return amxd_status_ok;
}

static int ppp_update_ipcp(amxd_object_t* obj,
                           amxc_var_t* args,
                           amxd_trans_t* trans) {
    int rv = -1;
    const char* device = GET_CHAR(args, "device");
    amxd_object_t* ipcp_obj = NULL;
    bool enabled = GET_BOOL(args, "enabled");
    ipcp_obj = amxd_object_findf(obj, "IPCP.");
    when_null_trace(ipcp_obj, exit, ERROR, "IPCP object for device %s not found", device);

    amxd_trans_select_object(trans, obj);
    if(GET_BOOL(args, "update_status")) {
        amxd_trans_set_value(cstring_t, trans, "Status", enabled ? "Up" : "Down");
        amxd_trans_set_value(cstring_t, trans, "ConnectionStatus", enabled ? "Connected" : "Disconnected");
    }
    amxd_trans_set_param(trans, "Name", GET_ARG(args, "ifname"));

    amxd_trans_select_object(trans, ipcp_obj);
    if(enabled) {
        amxd_trans_set_param(trans, "LocalIPAddress", GET_ARG(args, "LocalIPAddress"));
        amxd_trans_set_param(trans, "RemoteIPAddress", GET_ARG(args, "RemoteIPAddress"));
        amxd_trans_set_param(trans, "DNSServers", GET_ARG(args, "DNSServers"));
        amxd_trans_select_object(trans, obj);
        amxd_trans_set_value(cstring_t, trans, "LastConnectionError", "ERROR_NONE");
    } else {
        amxd_trans_set_value(cstring_t, trans, "LocalIPAddress", "");
        amxd_trans_set_value(cstring_t, trans, "RemoteIPAddress", "");
        amxd_trans_set_value(cstring_t, trans, "DNSServers", "");
    }

    rv = 0;
exit:
    return rv;
}

static int ppp_update_ipv6cp(amxd_object_t* obj,
                             amxc_var_t* args,
                             amxd_trans_t* trans) {
    int rv = -1;
    const char* device = GET_CHAR(args, "device");
    amxd_object_t* ipcp_obj = amxd_object_findf(obj, "IPv6CP.");
    bool enabled = GET_BOOL(args, "enabled_v6");
    when_null_trace(ipcp_obj, exit, ERROR, "IPv6CP object for device %s not found", device);

    amxd_trans_select_object(trans, obj);
    amxd_trans_set_param(trans, "Name", GET_ARG(args, "ifname"));

    amxd_trans_select_object(trans, ipcp_obj);
    if(enabled) {
        amxd_trans_set_param(trans, PPP_REMOTE_INTF_ID, GET_ARG(args, PPP_REMOTE_INTF_ID));
        amxd_trans_set_param(trans, PPP_LOCAL_INTF_ID, GET_ARG(args, PPP_LOCAL_INTF_ID));
        amxd_trans_select_object(trans, obj);
        amxd_trans_set_value(cstring_t, trans, "LastConnectionError", "ERROR_NONE");
    } else {
        amxd_trans_set_value(cstring_t, trans, PPP_REMOTE_INTF_ID, "");
        amxd_trans_set_value(cstring_t, trans, PPP_LOCAL_INTF_ID, "");
    }

    rv = 0;
exit:
    return rv;
}

static int ppp_update_pppoe(amxd_object_t* obj,
                            amxc_var_t* args,
                            amxd_trans_t* trans) {
    int rv = -1;
    const char* device = GET_CHAR(args, "device");
    amxd_object_t* pppoe_obj = amxd_object_findf(obj, "PPPoE.");
    when_null_trace(pppoe_obj, exit, ERROR, "PPPoE object for device %s not found", device);

    amxd_trans_select_object(trans, obj);
    amxd_trans_set_param(trans, "Name", GET_ARG(args, "ifname"));

    amxd_trans_select_object(trans, pppoe_obj);
    amxd_trans_set_param(trans, PPP_PPPOE_SESSION_ID, GET_ARG(args, PPP_PPPOE_SESSION_ID));

    rv = 0;
exit:
    return rv;
}

static int ppp_update_encryption(amxd_object_t* obj,
                                 amxc_var_t* args,
                                 amxd_trans_t* trans) {
    amxd_trans_select_object(trans, obj);
    amxd_trans_set_param(trans, PPP_ENCRYPTION, GET_ARG(args, PPP_ENCRYPTION));

    return 0;
}

static int ppp_update_authentication(amxd_object_t* obj,
                                     amxc_var_t* args,
                                     amxd_trans_t* trans) {
    amxd_trans_select_object(trans, obj);
    amxd_trans_set_param(trans, PPP_AUTHENTICATION, GET_ARG(args, PPP_AUTHENTICATION));

    return 0;
}

static int ppp_update_status(amxd_object_t* obj,
                             amxc_var_t* args,
                             amxd_trans_t* trans) {
    ppp_info_t* ppp_info = NULL;
    amxd_trans_select_object(trans, obj);
    amxd_trans_set_param(trans, PPP_STATUS, GET_ARG(args, PPP_STATUS));

    ppp_info = (ppp_info_t*) obj->priv;
    ppp_info->last_change = get_system_uptime();

    return 0;
}

static int ppp_update_connection_status(amxd_object_t* obj,
                                        amxc_var_t* args,
                                        amxd_trans_t* trans) {
    amxd_trans_select_object(trans, obj);
    amxd_trans_set_param(trans, PPP_CONNECTIONSTATUS, GET_ARG(args, PPP_CONNECTIONSTATUS));

    return 0;
}

static int ppp_update_last_connection_error(amxd_object_t* obj,
                                            amxc_var_t* args,
                                            amxd_trans_t* trans) {
    amxd_trans_select_object(trans, obj);
    amxd_trans_set_param(trans, PPP_LASTCONNECTIONERROR, GET_ARG(args, PPP_LASTCONNECTIONERROR));

    return 0;
}

int ppp_update_dm(UNUSED const char* function_name,
                  amxc_var_t* args,
                  UNUSED amxc_var_t* ret) {
    int rv = -1;
    amxd_object_t* obj = NULL;
    amxd_trans_t trans;
    const char* device = GET_CHAR(args, "device");
    ppp_info_t* ppp = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    when_str_empty_trace(device, exit, ERROR, "device is empty");

    obj = find_ppp_obj_by_device(device);
    if(obj == NULL) {
        SAH_TRACEZ_WARNING(ME, "Could not find PPP Interface object for device %s, storing data", device);
        dm_storage_store_data(device, args);
        rv = 0;
        goto exit;
    }

    if(amxc_var_get_key(args, "enabled", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        rv = ppp_update_ipcp(obj, args, &trans);
    }
    if(amxc_var_get_key(args, "enabled_v6", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        rv = ppp_update_ipv6cp(obj, args, &trans);
    }
    if(amxc_var_get_key(args, PPP_PPPOE_SESSION_ID, AMXC_VAR_FLAG_DEFAULT) != NULL) {
        rv = ppp_update_pppoe(obj, args, &trans);
    }
    if(amxc_var_get_key(args, PPP_ENCRYPTION, AMXC_VAR_FLAG_DEFAULT) != NULL) {
        rv = ppp_update_encryption(obj, args, &trans);
    }
    if(amxc_var_get_key(args, PPP_AUTHENTICATION, AMXC_VAR_FLAG_DEFAULT) != NULL) {
        rv = ppp_update_authentication(obj, args, &trans);
    }
    if(amxc_var_get_key(args, PPP_STATUS, AMXC_VAR_FLAG_DEFAULT) != NULL) {
        rv = ppp_update_status(obj, args, &trans);
    }
    if(amxc_var_get_key(args, PPP_CONNECTIONSTATUS, AMXC_VAR_FLAG_DEFAULT) != NULL) {
        rv = ppp_update_connection_status(obj, args, &trans);
    }
    if(amxc_var_get_key(args, PPP_LASTCONNECTIONERROR, AMXC_VAR_FLAG_DEFAULT) != NULL) {
        rv = ppp_update_last_connection_error(obj, args, &trans);
    }

    ppp = (ppp_info_t*) obj->priv;
    when_null_trace(ppp, exit, ERROR, "Private data is null");

    when_failed_trace(amxd_trans_apply(&trans, ppp_get_dm()), exit, ERROR,
                      "Failed to update dm for PPP Interface on device %s", device);

    if(amxc_var_get_key(args, "enabled", AMXC_VAR_FLAG_DEFAULT) != NULL) {

        // Stop enable/disable timers, we received an update
        stop_ppp_timer(ppp, true);
        stop_ppp_timer(ppp, false);

        // If Enable == true in the datamodel, and the down script got called,
        // try to re-enable the connection.
        bool enabled = GET_BOOL(args, "enabled");
        if(amxd_object_get_value(bool, obj, "Enable", NULL) && !enabled) {
            rv = mod_ppp_execute_function("update-config", ppp);
            when_failed_trace(rv, exit, ERROR,
                              "Failed to update config for PPP Interface on device %s", device);
            when_failed(start_ppp_timer(ppp, true), exit)
        }
    }
    rv = 0;

exit:
    amxd_trans_clean(&trans);
    return rv;
}


void dm_storage_store_data(const char* key, amxc_var_t* data) {
    amxc_var_set_key(&dm_storage, key, data, AMXC_VAR_FLAG_COPY | AMXC_VAR_FLAG_UPDATE);
}

amxc_var_t* dm_storage_get_data(const char* key) {
    return amxc_var_take_key(&dm_storage, key);
}

void dm_storage_init(void) {
    amxc_var_init(&dm_storage);
    amxc_var_set_type(&dm_storage, AMXC_VAR_ID_HTABLE);
}

void dm_storage_clean(void) {
    amxc_var_clean(&dm_storage);
}

uint32_t get_system_uptime(void) {
    uint32_t uptime = 0;
    struct sysinfo info;
    when_failed_trace(sysinfo(&info), exit, ERROR, "Could not read total uptime of the system");
    uptime = info.uptime;
exit:
    return uptime;
}
