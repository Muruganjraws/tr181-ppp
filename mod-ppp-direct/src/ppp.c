/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>

#include <amxm/amxm.h>

#include "ppp.h"
#include "service_ppp.h"

static int update_config(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    int retval = -1;
    when_null(args, exit);
    when_false(ppp_service_update(args), exit);

    retval = 0;
exit:
    return retval;
}

static int reset(UNUSED const char* function_name,
                 UNUSED amxc_var_t* args,
                 UNUSED amxc_var_t* ret) {
    ppp_service_terminate();
    return 0;
}

static int ppp_handle_script(const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t* tmp = NULL;
    const char* status = NULL;
    const char* ifname = NULL;
    amxc_var_t dns_list;
    amxc_string_t dns_string;

    SAH_TRACEZ_INFO(ME, "Function called - %s", function_name);

    amxc_var_init(&data);
    amxc_var_init(&dns_list);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&dns_list, AMXC_VAR_ID_LIST);

    amxc_string_init(&dns_string, 0);

    status = GET_CHAR(args, "status");
    ifname = GET_CHAR(args, "IFNAME");
    when_str_empty_trace(status, exit, ERROR, "Status must not be empty");
    when_str_empty_trace(ifname, exit, ERROR, "IFNAME must not be empty");

    amxc_var_add_key(bool, &data, "enabled", strcmp(status, "up") == 0);
    amxc_var_add_key(bool, &data, "update_status", true);
    amxc_var_add(cstring_t, &dns_list, GET_CHAR(args, "DNS1"));
    amxc_var_add(cstring_t, &dns_list, GET_CHAR(args, "DNS2"));
    amxc_string_csv_join_var(&dns_string, &dns_list);
    tmp = amxc_var_add_new_key(&data, "DNSServers");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&dns_string));
    amxc_var_add_key(cstring_t, &data, "LocalIPAddress", GET_CHAR(args, "IPLOCAL"));
    amxc_var_add_key(cstring_t, &data, "RemoteIPAddress", GET_CHAR(args, "IPREMOTE"));
    amxc_var_add_key(cstring_t, &data, "ifname", ifname);
    amxc_var_add_key(cstring_t, &data, "device", GET_CHAR(args, "DEVICE"));

    retval = amxm_execute_function("self", MOD_CORE, "update-dm", &data, ret);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Mod %s, func 'update-dm' returned %d",
                           MOD_CORE, retval);
    }

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&dns_list);
    amxc_string_clean(&dns_string);
    return retval;
}

static int use_netdev_subscriptions(const char* function_name,
                                    UNUSED amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "Function called - %s", function_name);
    return 1;
}

static int use_timers(const char* function_name,
                      UNUSED amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "Function called - %s", function_name);
    return 1;
}

static AMXM_CONSTRUCTOR ppp_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_PPP_CTRL);
    amxm_module_add_function(mod, "update-config", update_config);
    amxm_module_add_function(mod, "reset", reset);
    amxm_module_add_function(mod, "handle-script", ppp_handle_script);
    amxm_module_add_function(mod, "use-netdev-subscriptions", use_netdev_subscriptions);
    amxm_module_add_function(mod, "use-timers", use_timers);

    return 0;
}

static AMXM_DESTRUCTOR ppp_stop(void) {
    return 0;
}
