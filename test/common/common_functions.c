/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "ppp.h"
#include "dm_ppp.h"

#include "../common/common_functions.h"

// 'empty' replacements for functions in mod-dmext.so
static int _matches_regexp(void) {
    return AMXB_STATUS_OK;
}

static int _is_valid_ipv4(void) {
    return AMXB_STATUS_OK;
}

static int _is_valid_ipv6(void) {
    return AMXB_STATUS_OK;
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_status_t _dummy_dmstats_func(UNUSED amxd_object_t* object,
                                         UNUSED amxd_param_t* param,
                                         UNUSED amxd_action_t reason,
                                         UNUSED const amxc_var_t* const args,
                                         UNUSED amxc_var_t* const retval,
                                         UNUSED void* priv) {

    return amxd_status_ok;
}

static void dummy_resolve_mod_dmstat_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_read",
                                            AMXO_FUNC(_dummy_dmstats_func)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_list",
                                            AMXO_FUNC(_dummy_dmstats_func)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_object_describe",
                                            AMXO_FUNC(_dummy_dmstats_func)), 0);
}

void resolver_add_all_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_is_empty_or_in",
                                            AMXO_FUNC(_check_is_empty_or_in)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "check_is_ncp_in",
                                            AMXO_FUNC(_check_is_ncp_in)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "validate_delay",
                                            AMXO_FUNC(_validate_delay)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Reset",
                                            AMXO_FUNC(_Reset)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "matches_regexp",
                                            AMXO_FUNC(_matches_regexp)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ipv4",
                                            AMXO_FUNC(_is_valid_ipv4)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ipv6",
                                            AMXO_FUNC(_is_valid_ipv6)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ppp_interface_added",
                                            AMXO_FUNC(_ppp_interface_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ppp_interface_changed",
                                            AMXO_FUNC(_ppp_interface_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ppp_pppoe_interface_changed",
                                            AMXO_FUNC(_ppp_pppoe_interface_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ppp_interface_lowerlayers_changed",
                                            AMXO_FUNC(_ppp_interface_lowerlayers_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ppp_interface_removed",
                                            AMXO_FUNC(_ppp_interface_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "update",
                                            AMXO_FUNC(_update)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ppp_interface_enable_changed",
                                            AMXO_FUNC(_ppp_interface_enable_changed)), 0);

    dummy_resolve_mod_dmstat_functions(parser);
}
