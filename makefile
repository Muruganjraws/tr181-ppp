include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C mod-ppp-uci/src all
	$(MAKE) -C mod-ppp-direct/src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C mod-ppp-uci/src clean
	$(MAKE) -C mod-ppp-direct/src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ppp-uci.so $(DEST)/usr/lib/amx/ppp/modules/mod-ppp-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ppp-direct.so $(DEST)/usr/lib/amx/ppp/modules/mod-ppp-direct.so
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)
	$(foreach odl,$(wildcard odl/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.d
	$(foreach odl,$(wildcard odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.d/;)
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/ppp-up.sh $(DEST)/etc/ppp/ip-up.d/ppp-up.sh
	$(INSTALL) -D -p -m 0755 scripts/ppp-down.sh $(DEST)/etc/ppp/ip-down.d/ppp-down.sh

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ppp-uci.so $(PKGDIR)/usr/lib/amx/ppp/modules/mod-ppp-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ppp-direct.so $(PKGDIR)/usr/lib/amx/ppp/modules/mod-ppp-direct.so
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 odl/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/ppp-up.sh $(PKGDIR)/etc/ppp/ip-up.d/ppp-up.sh
	$(INSTALL) -D -p -m 0755 scripts/ppp-down.sh $(PKGDIR)/etc/ppp/ip-down.d/ppp-down.sh
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard odl/*.odl))
	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard odl/defaults.d/*.odl))

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C mod-ppp-uci/test run
	$(MAKE) -C mod-ppp-uci/test coverage
	$(MAKE) -C mod-ppp-direct/test run
	$(MAKE) -C mod-ppp-direct/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test