#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}
name="tr181-ppp"
name_pid="`pgrep ${name}`"

case $1 in
    start|boot)
        if [ -z "${name_pid}" ]; then
            ${name} -D
        fi
        ;;
    stop)
        if [ -n "${name_pid}" ]; then
            kill ${name_pid}
        fi
        ;;
    debuginfo)
        ubus-cli "PPP.?"
        ;;
    restart)
        $0 stop
        sleep 1
        $0 start
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|restart]"
        ;;
esac
